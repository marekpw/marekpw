
/**
 * Include the vendor stylesheets.
 * These will be extracted down to the valid .css files using extract-text-webpack-plugin.
 */
require('./stylesheet/vendor.scss');

/**
 * Vendor library manifest - please keep it updated and remove any unnecessary libraries.
 *
 * Note: Whenever this file is changed, a new vendor file is produced by Webpack.
 * Make sure this file doesn't change often, as it will enforce users to re-download the entire vendor package!
 */
import 'angular2/core';
import 'angular2/router';
import 'angular2/common';
import 'angular2/platform/browser';
import 'angular2/http';

/**
 * rxjs operators and libraries
 */
import 'rxjs/Observable';
import 'rxjs/add/operator/map';