/*
 * Main application component
 */

import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES, RouteConfig} from 'angular2/router';

import {DashboardComponent} from './dashboard/dashboard.component';

@Component({
    selector: 'app',
    directives: [ROUTER_DIRECTIVES],
    template: require('./app.html')
})
@RouteConfig([
    {
        path: '/dashboard',
        component: DashboardComponent,
        name: 'Index'
    }
])
export class AppComponent
{
    public constructor()
    {

    }
}