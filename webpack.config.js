
// load .env variables so they're properly transcribed
require('dotenv').load();

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        'src.app': './src/entry.ts',
        'src.vendor': './src/vendor.ts',
        'src.polyfills': './src/polyfills.ts'
    },

    output: {
        path: root('public'),
        filename: '[name]-[chunkhash].js',
        sourceMapFilename: '[name].map',
        chunkFilename: '[id].chunk.js',
        publicPath: process.env.APP_BASE_HREF
    },

    resolve: {
        extensions: [
            '', '.ts', '.js', '.json', '.css', '.scss', '.html',
            '.async.ts', '.async.js', '.async.json'
        ]
    },

    node: {
        fs: "empty"
    },

    ts: {
        transpileOnly: true
    },

    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
            {
                test: /\.async\.ts$/,
                loaders: ['es6-promise-loader', 'ts-loader'],
                exclude: [/\.(spec|e2e)\.ts$/]
            },
            {
                test: /\.ts/,
                loader: 'ts-loader',
                exclude: [/\.(spec|e2e|async)\.ts$/]
            },
            {
                test: /\.scss/,
                loader: ExtractTextPlugin.extract('style-loader', 'css!sass')
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg|gif)$/,
                loader: 'url-loader?limit=10'
            }
        ]
    },

    plugins: [
        // disabled because it causes errors in webpack output
        //new webpack.optimize.OccurenceOrderPlugin(true),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['src.vendor', 'src.polyfills'],
            minChunks: Infinity
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: true
        }),

        new webpack.EnvironmentPlugin([
            'APP_BASE_HREF'
        ]),

        new ExtractTextPlugin('[name]-[contenthash].css')
    ]
};

function root(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return path.join.apply(path, [__dirname].concat(args));
}